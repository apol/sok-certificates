import os
import subprocess
import re

for subdir, dirs, files in os.walk('./'):
    for file in files:
      if re.match("sok-.*.svg$", file):
        subprocess.call(["inkscape", file, "--export-filename="+file.replace(".svg", ".pdf")])
