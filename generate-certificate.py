import sys
import subprocess

if len(sys.argv)<4:
    print("usage: generate-certificate.py <name> <project-line1> <project-line2>")
    sys.exit(1)

name = sys.argv[1]
projectline1 = sys.argv[2]
projectline2 = sys.argv[3]
year = "2023"

f = open('sok.svg')
filecontents = f.read()

filecontents=filecontents.replace('$YEAR', year)
filecontents=filecontents.replace('$NAME', name)
filecontents=filecontents.replace('$PROJECTLINE1', projectline1)
filecontents=filecontents.replace('$PROJECTLINE2', projectline2)

outputpath = 'sok-%s-%s.' % (year, name.replace('/', '-'))
towrite=open(outputpath+"svg", 'w')
towrite.write(filecontents)
towrite.close()
